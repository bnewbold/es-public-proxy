
.PHONY: help
help: ## Print info about all commands
	@echo "Commands:"
	@echo
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[01;32m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## Run all tests
	cargo test

.PHONY: dev
dev: ## Run service locally (unsafe mode)
	cargo run -- --unsafe-all-indices

.PHONY: lint
lint: ## Run syntax/style checks
	cargo check
	cargo clippy

.PHONY: fmt
fmt: ## Run syntax re-formatting
	cargo fmt

.PHONY: build
build: ## Build (debug)
	cargo build

extra/es-public-proxy.1: extra/es-public-proxy.1.scdoc
	scdoc < extra/es-public-proxy.1.scdoc > extra/es-public-proxy.1

.PHONY: manpage
manpage: extra/es-public-proxy.1 ## Rebuild manpage using scdoc

.PHONY: deb
deb: ## Build debian packages (.deb files)
	cargo deb
