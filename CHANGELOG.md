
# CHANGELOG

## [UNRELEASED]

Defined a MRSV of 1.49.

## [0.2.6] - 2021-03-29

### Changed

- basic index operations (GET, HEAD, OPTIONS) allowed, but only if
  `unsafe_all_indices` is disabled. This allows "index exists" requests using
  the ES client.
- `Via` header now has "HTTP/1.1" prefix instead of "1.1". This makes the
  purpose version number clearer; both formats allowed according to MDN docs.
- updated dependencies. notably, tokio 1.4

## [0.2.5] - 2020-12-23

### Changed

- Debian package is now enabled (and by default, started) on installation
- updated dependencies to hyper 0.14 and tokio 1.0

## [0.2.4] - 2020-12-17

### Fixed

- sort order field as string or array
- bug resulting in some single-line-body test cases not running
- allow scrolling via GET requests

## [0.2.3] - 2020-12-17

### Added

- `date_historgram` aggregations
- `composite` aggregations

## [0.2.2] - 2020-12-17

### Added

- support for `filters` aggregation type

## [0.2.1] - 2020-10-18

### Fixed

- filter queries can be single or an array
- allow numeric and boolean values in term queries (eg, in filters)

## [0.2.0] - 2020-08-26

Initial public release.
