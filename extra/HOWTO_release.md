
Reminder of release process (new tagged versions):

final prep:
- consider running `cargo update` and checking that everything works
- `make lint` and fix anything
- `make fmt` and commit any fixes
- update CHANGELOG and increment version in `Cargo.toml`. `make build` to
  update `Cargo.lock`, then commit
- `make test`, `make deb` and ensure build works and is clean
- ensure working directory is *entirely* clean (no "Untracked files" or edits,
  even if unrelated to code changes)

push/tag/etc:
- push to crates.io: `cargo publish`
- create a git tag: `git tag vX.Y.Z -a -s -u bnewbold@archive.org -m "bump version to X.Y.Z"`
- push git and tag: `git push`, `git push --tags`
- if still supporting ubuntu xenial, do .deb build on a xenial machine
- upload deb package, eg: `ia upload es-public-proxy-deb ./target/debian/es-public-proxy_X.Y.Z_amd64.deb`

deploy process (archive.org services):
- can just download .deb and install (`sudo apt install ./es-public-proxy_X.Y.Z_amd64.deb`)
- update ansible roles with new version so it gets deployed in the future
