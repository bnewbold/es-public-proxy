use hyper::{Body, Method, Request};
use std::io::BufRead;
use std::path::Path;

pub struct ExampleParts {
    pub method: String,
    pub path_and_query: String,
    pub body: Option<String>,
}

pub fn load_parts(path: &Path) -> ExampleParts {
    let file = std::fs::File::open(path).unwrap();
    let mut lines = std::io::BufReader::new(file).lines();
    let first_line: Vec<String> = lines
        .next()
        .unwrap()
        .unwrap()
        .split(" ")
        .map(|v| v.into())
        .collect();
    let body: Vec<String> = lines
        .map(|v| v.into())
        .collect::<Result<Vec<String>, _>>()
        .unwrap();
    let body: Option<String> = if body.len() == 0 {
        None
    } else {
        Some(body.join("\n"))
    };

    ExampleParts {
        method: first_line[0].clone(),
        path_and_query: first_line[1].clone(),
        body: body,
    }
}

pub fn load_request(path: &Path) -> Request<Body> {
    let parts = load_parts(path);
    Request::builder()
        .uri(parts.path_and_query)
        .method(
            Method::from_bytes(parts.method.as_bytes()).expect("valid method in example text file"),
        )
        .body(match parts.body {
            Some(data) => Body::from(data),
            None => Body::empty(),
        })
        .expect("constructing upstream request")
}
